/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examencorte1;

/**
 *
 * @author delap
 */
public class Administrativos extends Empleados {
    private String tipoDeContrato;
    
    //Constructores.

    public Administrativos(String tipoDeContrato, int numEmpleados, String nombre, String domicilio, float pagoPorDia, int diasTrabajados, float ISR) {
        super(numEmpleados, nombre, domicilio, pagoPorDia, diasTrabajados, ISR);
        this.tipoDeContrato = tipoDeContrato;
    }
    
    //Get and Set.

    public String getTipoDeContrato() {
        return tipoDeContrato;
    }

    public void setTipoDeContrato(String tipoDeContrato) {
        this.tipoDeContrato = tipoDeContrato;
    } 

    @Override
    public float calcularPago() {
        return this.getDiasTrabajados() * this.getPagoPorDia();
    }

    @Override
    public float calcularImpuesto() {
       float pago = 0;
       if (this.calcularPago()< 5000){
           pago = this.calcularPago()*.05f;
       }else if(this.calcularPago()>=5000){
           pago = this.calcularPago()*0.08f;
       }else if(this.calcularPago()>= 10000){
          pago = this.calcularPago()*0.10f; 
       }
       else{
           pago = this.calcularPago();
       }
       return pago;
    }
    
    
}
