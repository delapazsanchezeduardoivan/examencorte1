/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examencorte1;

/**
 *
 * @author delap
 */
public abstract class Empleados {
    protected int numEmpleados;
    protected String nombre;
    protected String domicilio;
    protected float pagoPorDia;
    protected int diasTrabajados;
    protected float ISR;
    
    
    //Constructores.

    public Empleados(int numEmpleados, String nombre, String domicilio, float pagoPorDia, int diasTrabajados, float ISR) {
        this.numEmpleados = numEmpleados;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagoPorDia = pagoPorDia;
        this.diasTrabajados = diasTrabajados;
        this.ISR = ISR;
    }

    public Empleados(){
        this.numEmpleados = 0;
        this.nombre = " ";
        this.domicilio = " ";
        this.diasTrabajados = 0;
        this.pagoPorDia = 0.0f;
        this.ISR = 0.0f;
    }
    // Get and Set.
    public int getNumEmpleados() {
        return numEmpleados;
    }

    public void setNumEmpleados(int numEmpleados) {
        this.numEmpleados = numEmpleados;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getPagoPorDia() {
        return pagoPorDia;
    }

    public void setPagoPorDia(float pagoPorDia) {
        this.pagoPorDia = pagoPorDia;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public float getISR() {
        return ISR;
    }

    public void setISR(float ISR) {
        this.ISR = ISR;
    }
    
    public abstract float calcularPago();
    
    public abstract float calcularImpuesto();
    
    
}
