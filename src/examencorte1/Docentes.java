/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examencorte1;

/**
 *
 * @author delap
 */
public class Docentes extends Empleados{
    private String nivel;
    
    //Constructor.

    public Docentes() {
        super();
        this.nivel = "";
    }

    public Docentes(String nivel, int numEmpleados, String nombre, String domicilio, float pagoPorDia, int diasTrabajados, float ISR) {
        super(numEmpleados, nombre, domicilio, pagoPorDia, diasTrabajados, ISR);
        this.nivel = nivel;
    }
    
    //Get and Set.

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
    
    @Override
    public float calcularPago() {
        return this.getPagoPorDia() * this.getDiasTrabajados();
    }
    
    public float calcularPagoTotal() {
        float bono = 0;
        float pago = 0;
        if (this.nivel == "1" ){
           bono = this.calcularPago()*.35f;
           pago = this.calcularPago() + bono;
        }else if(this.nivel == "2"){
           bono = this.calcularPago()*.40f;
           pago = this.calcularPago() + bono;
       } else if(this.nivel == "3"){
           bono = this.calcularPago()*.50f;
           pago = this.calcularPago() + bono;
       }
       else{
           pago = this.calcularPago();
       }
       return pago;
    }

    @Override
    public float calcularImpuesto() {
        return this.calcularPagoTotal() * (this.getISR()/100);
    }

}
